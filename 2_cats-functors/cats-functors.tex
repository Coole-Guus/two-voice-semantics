\documentclass[../two-voice-semantics.tex]{subfiles}

\begin{document}

\beginchapter{2}{Categories and Functors}{cats-functors}


\section{Motivation}
\label{sec:cats-motivation}

Recall that we used in \cref{sec:denotational-arith} the set $D$ defined by
\begin{equation*}
  D = \QNN \cup \set{\err}
\end{equation*}
to give denotational semantics to arithmetic expressions in form of a map
$\denArith{-} \from \Exp \to D$, where the element $\err$ expresses failure.
In the course of defining this map, we had to make case distinctions of the following form.
\begin{equation}
  \label{eq:den-arith-plus}
  \denArith{e_{1} \oplus e_{2}} =
  \begin{cases}
    \denArith{e_{1}} + \denArith{e_{2}},
    & \denArith{e_{k}} \neq \err \text{ for all } k \in \set{1,2} \\
    \err, & \denArith{e_{k}} = \err \text{ for some } k \in \set{1,2}
  \end{cases}
\end{equation}
This is very tedious, error-prone and not very enlightening.
Any professional software engineer would scream at us that we should abstract from the
repetition!
Such abstraction requires support from a high-level language.
Category theory has its origin in algebraic topology where the theory became so complex
that it was not possible any longer to handle it with basic set theory, the assembly language
of mathematics.
This new theory turned out to be so powerful that it could be applied in mathematics in
general and create bridges between areas that were unthinkable before.
Because of its powerful abstraction mechanisms, category theory has also found its way into
various areas of computer science and even programming languages like Haskell.

What is this wondrous thing called category theory then?
If I were pressed to sum it up in one sentence, then I would say:
``Category theory is the study of relations between mathematical objects and their
structure preserving transformations.''
However, this is so abstract that this description will only be meaningful once you know
what category theory is, at which point you do not need a summary any longer.
The only way to learn category theory, as anything, is by doing it.
This is why we will take a pragmatic approach here and introduce concepts of category theory
according to the problems that we have to solve.
Eventually this will give you a picture of the main ideas of category theory.

With this approach in mind, let us come back to our denotational semantics for \Arith{} with
failure and see how we can reformulate the definition.
Looking at \cref{eq:den-arith-plus}, we see that it does two things: if the outcome of
$\denArith{e_{k}}$ for either $k \in \set{1,2}$ yields an error, then this error is propagated;
otherwise we use an operation on $\QNN$ to combine their result.
Zooming out a bit, we are taking the map $f \from \QNN \times \QNN \to D$ given by
$f(a,b) = a + b$ and \keyw*{extend} it to a map $\ext{f} \from D \times D \to D$ by the
following case distinction.
\begin{equation*}
  \ext{f}(x, y) =
  \begin{cases}
    f(x, y), & x \neq \err \text{ and } y \neq \err \\
    \err, & x = \err \text{ or } y = \err
  \end{cases}
\end{equation*}
We then apply this map to the pair $(\denArith{e_{1}}, \denArith{e_{2}})$ to obtain the semantics:
\begin{equation*}
  \denArith{e_{1} \oplus e_{2}} = \ext{f}\parens*{\denArith{e_{1}}, \denArith{e_{1}}}
\end{equation*}

How does this reformulation help us?
It separates two concerns: on the one hand, the structural extension of $\QNN$ with failures
and handling the those failures, and, on the other hand, the specification of semantics as
operations on values in $\QNN$.
This approach is an instance of the following \keyw*{extension problem}.
\begin{definition}{}{ext-problem}
  Let $\Maps{J}$ be a collection of maps.
  A map $i \from A \to B$ in $\Maps{J}$ is said to have the \keyw{extension property} with respect
  to $\Maps{J}$ if for all maps $f \from A \to C$ in $\Maps{J}$ there a $g \from B \to C$ with
  $g \comp i = f$.
  We say that the following diagram \keyw[commuting diagram]{commutes}.
  \begin{equation*}
    % file:///home/henning/opt/quiver/src/index.html?q=WzAsMyxbMCwwLCJCIl0sWzAsMSwiQSJdLFsxLDAsIkMiXSxbMSwyLCJmIiwyXSxbMSwwLCJpIl0sWzAsMiwiZyIsMCx7InN0eWxlIjp7ImJvZHkiOnsibmFtZSI6ImRhc2hlZCJ9fX1dXQ==
    \begin{tikzcd}
      B & C \\
      A
      \arrow["f"', from=2-1, to=1-2]
      \arrow["i", from=2-1, to=1-1]
      \arrow["g", dashed, from=1-1, to=1-2]
    \end{tikzcd}
  \end{equation*}
\end{definition}

We can apply \cref{def:ext-problem} to our semantics as follows.
Let $i \from \QNN \to D$ be the inclusion map given by $i(a) = a$ and define the collection
$\Maps{B}$ to contain all maps that have a \keyw*{base point} in their codomain:
\begin{equation*}
  \Maps{B} = \setDef{f \from X \to Y}{Y \text{ has a distinguished point } y_{0}} \, .
\end{equation*}

\begin{lemma}{}{simple-ext-based}
  The inclusion $i$ has the extension property with respect to $\Maps{B}$.
\end{lemma}
\begin{proof}
  Given $f \from \QNN \to Y$ in $\Maps{B}$, we define $g \from D \to Y$ by
  \begin{equation*}
    g(x) =
    \begin{cases}
      f(x), & x \in \QNN \\
      y_{0}, & x = \err
    \end{cases} \, ,
  \end{equation*}
  where $y_{0}$ is the base point of $Y$.
  Clearly, for all $a \in \QNN$ we have $(g \comp i)(a) = f(a)$ and thus $g \comp i = f$ holds.
\end{proof}

This is a good first step, but we can do better.
Let us declare $\err$ to be the base point of $D$.
Note that our extension $g$ in \cref{lem:simple-ext-based} has the special property that it
preserves the base point in the sense that $g(\err) = y_{0}$.
We call $g$ a \keyw*{based map} between \keyw*[based set]{based sets}.
Not only that, it is even \emph{unique} among all based maps that extend $f$ in the sense of
\cref{def:ext-problem}.
That is, if $h \from D \to Y$ is another based map with $h \comp i = f$, then $h = g$.
To formulate this property we will need to refine \cref{def:ext-problem} by using category theory.

\input{cats}
\input{functors}
\input{further}


\dobib{}

\end{document}

% Local Variables:
% ispell-local-dictionary: "british"
% mode: latex
% TeX-engine: xetex
% End: