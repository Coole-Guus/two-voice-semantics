\section{Categories}
\label{sec:cats}

The starting point of category theory are categories, which formalise the notion of relation
between mathematical objects.
Integral to category theory is the realisation that the main property of relations is that they
can be composed and that, in order to work with diagrams like in \cref{def:ext-problem},
we have to make sure that composition of relations is independent of how we follow paths
in a diagram.

\begin{definition}{}{category}
  A \keyw{category} $\CC$ consists of a collection $\ob{\CC}$ of \keyw[object]{objects} and
  for all objects $A$ and $B$ in $\ob*{\CC}$ a collection $\CC(A, B)$ of
  \keyw[morphism]{morphisms} from $A$ to $B$ subject to the four conditions below.
  As shorthand, we write $A \in \CC$ if $A$ is an object in $\ob{\CC}$ and
  $f \from A \to B$ if $f$ is a morphism in $\CC(A, B)$.
  \begin{enumerate}
  \item For all $A \in \CC$ there is a morphism $\id_{A} \from A \to A$ in $\CC(A,A)$, called
    \keyw{identity} on $A$.
  \item There is a \keyw{composition} map that assigns to all pairs of morphisms $f \from A \to B$
    and $g \from B \to C$ a morphism $g \comp f \from A \to C$ (read ``$g$ composed with $f$'' or
    ``$g$ after $f$).
  \item Identity is neutral: for all morphisms $f \from A \to B$ the two \keyw{unit axioms} hold:
    \begin{equation*}
      \id_{B} \comp f = f \quad \text{ and } \quad f \comp \id_{A}
    \end{equation*}
  \item Composition is associative: for all morphisms $h \from C \to D$, $g \from B \to C$ and
    $f \from A \to B$ the \keyw{associativity axiom} holds:
    \begin{equation*}
      h \comp (g \comp f) = (h \comp g) \comp f
    \end{equation*}
  \end{enumerate}
  \vspace*{-1ex}
  For a morphism $f \in \CC(A, B)$ in a category $\CC$, we call the object $A$ the \keyw{domain} and
  $B$ the \keyw{codomain} of $f$.
\end{definition}

\begin{example}{}{set-categories}
  \begin{enumerate}
  \item The most familiar example is the category $\SetC$ with sets as objects and maps as morphisms.
    As identity we take the identity map $\id_{A} \from A \to A$ with $\id_{A}(x) = x$ and
    composition is sequential application of maps, as in $(g \comp f)(x) = g(f(x))$.
    It is easy to check that the unit and associativity axioms hold.
  \item For our motivating example, we will use the category $\BSetC$ of based sets and based maps.
    This category has as object in $\ob{\BSetC}$ pairs $(X, x_{0})$ of a set $X$ and a base point
    $x_{0} \in X$.
    A morphism $f \from (X, x_{0}) \to (Y, y_{0})$ is a based map between the underlying sets,
    that is, the morphisms are given by
    \begin{equation*}
      \BSetC((X, x_{0}), (Y, y_{0})) =  \setDef{f \in \SetC(X, Y)}{f(x_{0}) = y_{0}} \, .
    \end{equation*}
    It is easy to see that the identity is based and that based maps are closed under
    composition of maps.
    Thus, identities and composition are given as in $\SetC$.
  \item Morphisms in categories do not have to be maps.
    For instance, we let $\RelC$ denote the category that has sets as objects and \emph{relations}
    $R \subseteq A \times B$ as morphisms $A \to B$.
    The identities are given by diagonal relations
    \begin{equation*}
      \id_{A} = \setDef{(x,x)}{x \in A}
    \end{equation*}
    and composition by the usual composition of relations $R \from A \to B$ and $S \from B \to C$:
    \begin{equation*}
      S \comp R = \setDef{(x,z)}{(x, y) \in R \text{ and } (y, z) \in S} \, .
    \end{equation*}
  \item Also objects of categories do not have to be some kind of set.
    Recall that a preorder relation is a relation $R \subseteq X \times X$ on a set $X$ that is
    \begin{itemize}
    \item \keyw{reflexive}: if $(x,x) \in R$ for all $x \in X$; and
    \item \keyw{transitive}: if $(x,y) \in R$ and $(y,z) \in R$, then $(x,z) \in R$.
    \end{itemize}
    Such a relation can be considered as a category $\Cat{R}$ by taking $\ob{\Cat{R}} = X$
    and
    \begin{equation*}
      \Cat{R}(x,y) =
      \begin{cases}
        \emptyset, & (x,y) \not\in R \\
        \ast, & (x,y) \in R
      \end{cases}
    \end{equation*}
    for some set $\ast$ with exactly one element, say $0$.
    In other words, there is a morphism $0 \from x \to y$ if and only if $(x,y) \in R$.
    The identity morphisms are given by this single element, which is possible since
    $\Cat{R}(x,x) = \ast$ by reflexivity.
    Composition of morphisms $x \to y$ and $y \to z$ is possible by transitivity of $R$.
    Since there is at most one morphism between objects in $\Cat{R}$, the unit and associativity
    axioms hold automatically.
  \end{enumerate}
\end{example}

\begin{exercise}
  Give detailed proofs that $\SetC$ and $\Cat{R}$ for a preorder relation $R$ from
  \cref{ex:set-categories} are categories.
\end{exercise}

\begin{exercise}
  \label{exerc:graph-category}
  A (directed) graph $G$ is a tuple $(N, E, s, t)$ of a set $N$ of nodes, a set $E$ of edges and
  maps $s \from E \to N$ and $t \from E \to N$ that return for an edge, respectively, its source
  and its target.
  By a graph homomorphism $f \from G_{1} \to G_{2}$ is a pair
  $(f_{0}, f_{1})$ of maps $f_{0} \from N_{1} \to N_{2}$ and $f_{1} \from E_{1} \to E_{2}$, such
  that $s_{2} \comp f_{1} = f_{0} \comp s_{1}$ and $t_{2} \comp f_{1} = f_{0} \comp t_{1}$.
  \begin{enumerate}
  \item Express the graph in the following diagram in this form.
    \begin{equation*}
      % file:///home/henning/opt/quiver/src/index.html?q=WzAsMyxbMCwwLCJhIl0sWzEsMCwiYiJdLFswLDEsImMiXSxbMCwxLCIiLDAseyJjdXJ2ZSI6LTF9XSxbMCwxLCIiLDIseyJjdXJ2ZSI6MX1dLFsyLDBdLFsxLDJdXQ==
      \begin{tikzcd}
        a & b \\
        c \arrow[loop right]
        \arrow[curve={height=-6pt}, from=1-1, to=1-2]
        \arrow[curve={height=6pt}, from=1-1, to=1-2]
        \arrow[from=2-1, to=1-1]
        \arrow[from=1-2, to=2-1]
      \end{tikzcd}
    \end{equation*}
  \item Give graph homomorphism from the graph above to the graph in the following diagram.
    \begin{equation*}
      % file:///home/henning/opt/quiver/src/index.html?q=WzAsMixbMCwwLCIwIl0sWzEsMCwiMSJdLFswLDEsIiIsMCx7ImN1cnZlIjotMX1dLFsxLDAsIiIsMCx7ImN1cnZlIjotMX1dXQ==
      \begin{tikzcd}
        0 & 1 \arrow[loop right]
        \arrow[curve={height=-6pt}, from=1-1, to=1-2]
        \arrow[curve={height=-6pt}, from=1-2, to=1-1]
      \end{tikzcd}
    \end{equation*}
  \item Prove that there is a category $\DiGrC$ of directed graphs and their homomorphisms.
  \end{enumerate}
\end{exercise}

What can we do with categories?
So far not much but show that some mathematical structures fit the definition.
However, we can already reason about relations between morphisms in categories.
These are often conveniently displayed as \keyw*[diagram]{diagrams}.
For instance, the following diagram shows three morphisms and the identity on $A$
in some category $\CC$.
\begin{equation*}
  % file:///home/henning/opt/quiver/src/index.html?q=WzAsNCxbMCwwLCJBIl0sWzEsMCwiQiJdLFswLDEsIkEiXSxbMSwxLCJDIl0sWzAsMSwiZiJdLFsxLDMsImciXSxbMiwzLCJoIiwyXSxbMCwyLCJcXGlkX3tBfSIsMl1d
\begin{tikzcd}
	A & B \\
	A & C
	\arrow["f", from=1-1, to=1-2]
	\arrow["g", from=1-2, to=2-2]
	\arrow["h"', from=2-1, to=2-2]
	\arrow["{\id_{A}}"', from=1-1, to=2-1]
\end{tikzcd}
\end{equation*}
We may ask for this diagram to \keyw*{commute} in $\CC$, which means that all parallel paths through
the diagram are equal when composed.
In the diagram above, this means that $g \comp f = h \comp \id_{A}$ holds in $\CC$.
Note that by the unit axiom we thus also have that the following diagram commutes.
\begin{equation*}
  % file:///home/henning/opt/quiver/src/index.html?q=WzAsNCxbMCwwLCJBIl0sWzEsMCwiQiJdLFsxLDEsIkMiXSxbMCwxLCJBIl0sWzAsMSwiZiJdLFsxLDIsImciXSxbMCwyLCJoIiwxXSxbMCwzLCJcXGlkX3tBfSIsMl0sWzMsMiwiaCIsMl1d
\begin{tikzcd}
	A & B \\
	A & C
	\arrow["f", from=1-1, to=1-2]
	\arrow["g", from=1-2, to=2-2]
	\arrow["h"{description}, from=1-1, to=2-2]
	\arrow["{\id_{A}}"', from=1-1, to=2-1]
	\arrow["h"', from=2-1, to=2-2]
\end{tikzcd}
\end{equation*}
By associativity we can also ask unambiguously for the following diagram to commute.
\begin{equation*}
  % file:///home/henning/opt/quiver/src/index.html?q=WzAsNixbMCwwLCJBIl0sWzEsMCwiQiJdLFsxLDEsIkMiXSxbMCwxLCJBIl0sWzIsMCwiRCJdLFsyLDEsIkUiXSxbMCwxLCJmIl0sWzEsMiwiZyJdLFswLDIsImgiLDFdLFswLDMsIlxcaWRfe0F9IiwyXSxbMywyLCJoIiwyXSxbMSw0LCJrIl0sWzIsNSwibSIsMl0sWzQsNSwibCJdXQ==
\begin{tikzcd}
	A & B & D \\
	A & C & E
	\arrow["f", from=1-1, to=1-2]
	\arrow["g", from=1-2, to=2-2]
	\arrow["h"{description}, from=1-1, to=2-2]
	\arrow["{\id_{A}}"', from=1-1, to=2-1]
	\arrow["h"', from=2-1, to=2-2]
	\arrow["k", from=1-2, to=1-3]
	\arrow["m"', from=2-2, to=2-3]
	\arrow["l", from=1-3, to=2-3]
\end{tikzcd}
\end{equation*}
In equations, this means that
\begin{align*}
  \SwapAboveDisplaySkip
  l \comp (k \comp f)
  & = (l \comp k) \comp f \\
  & = (m \comp g) \comp f \\
  & = m \comp (g \comp f) \\
  & = m \comp h \\
  & = m \comp (h \comp \id_{A})
\end{align*}
or just, since the parentheses are unnecessary because of associativity,
\begin{equation*}
  l \comp k \comp f = m \comp g \comp f = m \comp h \, .
\end{equation*}

In categories, we can now ask for certain diagrams to exist or to commute.
For instance, the notion of isomorphism is important in any mathematical theory
because isomorphic structures are typically considered exchangeable.
\begin{definition}{}{iso}
  Let $\CC$ be a category.
  A morphism $f \from A \to B$ in $\CC$ is an \keyw{isomorphism} if there is a morphism
  $g \from B \to A$ with $f \comp g = \id _{A}$ and $g \comp f = \id_{B}$.
  We call $g$ the \keyw{inverse} of $f$.
  If there is an isomorphism $A \to B$, then we say that $A$ and $B$ are \keyw{isomorphic}
  and we denote this by $A \cong B$.
\end{definition}

\begin{example}{}{basic-iso}
  In $\SetC$, the isomorphisms are exactly the bijective maps and in $\BSetC$ these have to
  additionally preserve the base point.
  Isomorphisms in the category $\DiGrC$ of graphs are the usual graph isomorphisms.
\end{example}

\begin{exercise}
  Let $\CC$ be a category and $f$ a morphism $A \to B$.
  We say for morphisms $g \from B \to A$ and $h \from B \to A$ that $g$ is a \keyw{preinverse} if
  $f \comp g = \id_{A}$ and $h$ a \keyw{postinverse} if $h \comp f = \id_{B}$.
  Prove that the following are equivalent in $\CC$.
  \begin{enumerate}
  \item $f$ is an isomorphism.
  \item $f$ admits a preinverse $g$ and a postinverse $h$.
  \item $f$ admits a preinverse $g$ and $g$ a preinverse $h$.
  \item $f$ admits a postinverse $h$ and $h$ a postinverse $g$.
  \end{enumerate}
\end{exercise}

\begin{exercise}
  \begin{enumerate}
  \item Prove that the inverse of an isomorphism in a category $\CC$ is unique.
  \item Prove that $\id_{A}$ is an isomorphism for all objects $A$ in a category $\CC$.
  \item Prove that the inverse of an isomorphism is also an isomorphism.
  \item Prove that if $A \cong B$ and $B \cong C$, then also $A \cong C$.
  \item Conclude from 2 - 4 that ``being isomorphic'' is an equivalence relation.
  \end{enumerate}
\end{exercise}

%%% Local Variables:
%%% ispell-local-dictionary: "british"
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "cats-functors"
%%% End: