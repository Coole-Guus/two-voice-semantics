\section{Functors}
\label{sec:functors}

When we work with some mathematical objects, such as based sets or graphs, we ask that their
morphisms are \keyw*{structure preserving} maps.
In the case of based sets the maps preserve base points, while in the case of graphs the
source and target of edges are preserved.
What is the structure of categories that should be preserved?
Of course, composition and identities!
Or, more generally, the morphisms of categories should preserve commutative diagrams.
With this in mind, we only need a good name for those morphisms and everything else follows
from this idea.

\begin{definition}{}{functor}
  A \keyw{functor} $F \from \CC \to \CD$ between categories $\CC$ and $\CD$ consists of a map
  $F_{0} \from \ob{\CC} \to \ob{\CD}$ and for all $A,B \in \ob{\CC}$ of maps
  $F_{A,B} \from \CC(A,B) \to \CD(F_{0}A, F_{0}B)$ with the property that
  \begin{enumerate}
  \item $F_{A,A}(\id_{A}) = \id_{F_{0}A}$, and
  \item $F_{A,C}(g \comp f) = F_{B,C}(g) \comp F_{A,B}(f)$.
  \end{enumerate}
  For readability, we leave out the subscript and write $F$ for all those maps.
\end{definition}

\begin{example}{}{basic-functors}
  \begin{enumerate}
  \item There is a functor $U \from \BSetC \to \SetC$ given as follows.
    \begin{equation*}
      U(X, x_{0}) = X \quad \text{ and } \quad Uf = f
    \end{equation*}
    This functor is called \keyw{forgetful functor}.
  \item Given a map $f \from X \to Y$, we can form its graph $\Gr f$ by
    \begin{equation*}
      \Gr f = \setDef{(x,y) \in X \times Y}{f(x) = y} \, .
    \end{equation*}
    This extends to a functor $\Gr \from \SetC \to \RelC$ if we define it on objects by
    $\Gr X = X$.
  \end{enumerate}
\end{example}

\begin{exercise}
  Provide the details for \cref{ex:basic-functors}.
\end{exercise}

\begin{exercise}
  Prove that functors preserve isomorphisms: Given a functor $F \from \CC \to \CD$ and an
  isomorphism $f \from A \to B$ in $\CC$, prove that $Ff$ is an isomorphism in $\CD$.
\end{exercise}

Using the language of functors, we can now give a better formulation to our extension problem.
\begin{lemma}{}{basepoint-extension}
  There is a functor $E \from \SetC \to \BSetC$ and for all $X \in \SetC$ a map
  $i_{X} \from X \to U(E X)$, such that for all $(Y, y_{0}) \in \BSetC$ and
  $f \from X \to U(Y, y_{0})$ there is a unique based map $\ext{f} \from EX \to (Y, y_{0})$ that
  renders the following diagram commutative.
  \begin{equation}
    \label{eq:basepoint-extension-diag}
    % file:///home/henning/opt/quiver/src/index.html?q=WzAsMyxbMCwwLCJVKEVYKSJdLFsxLDAsIlkgPSBVKFkseV8wKSJdLFswLDEsIlgiXSxbMiwxLCJmIl0sWzAsMiwiaV97WH0iXSxbMCwxLCJVIFxcZXh0e2Z9Il1d
    \begin{tikzcd}
      {U(EX)} & {U(Y,y_0)} \\
      X
      \arrow["f", from=2-1, to=1-2]
      \arrow["{i_{X}}", from=1-1, to=2-1]
      \arrow["{U \ext{f}}", from=1-1, to=1-2]
    \end{tikzcd}
  \end{equation}
\end{lemma}
\begin{proof}
  The main difficulty lies in adding a base point to a set that is not already in the set.
  There are many ways to do so, but here is one.
  We define $E$ on objects by
  \begin{equation*}
    EX = (\set{0} \cup \setDef{(1, x)}{x \in X}, 0)
  \end{equation*}
  and on morphisms $f \from X \to Y$ by the following case distinction.
  \begin{equation*}
    (Ef)(u) =
    \begin{cases}
      (1, f(x)), & u = (1, x) \\
      0, & u = 0
    \end{cases}
  \end{equation*}
  Preservation of identities is straightforward
  \begin{equation*}
    (E \, \id_{X})(u) =
    \begin{cases}
      (1, \id_{X}(x)), & u = (1, x) \\
      0, & u = 0
    \end{cases}
    = u
    = \id_{EX}(u)
  \end{equation*}
  and also preservation of composition is not much more difficult:
  \begin{align*}
    E(g \comp f)(u)
    & =
    \begin{cases}
      (1, g(f(x)), & u = (1, x) \\
      0, & u = 0
    \end{cases}
    \\
    & =
    \begin{cases}
      (1, g(y)), & u = (1, x), f(x) = y \\
      0, & u = 0
    \end{cases}
    \\
    & =
    \begin{cases}
      (Eg)(1, f(x)), & u = (1, x) \\
      0, & u = 0
    \end{cases}
    \\
    & = (Eg \comp Ef)(u)
  \end{align*}
  The inclusion map $i_{X} \from X \to U(EX)$ is given by $i_{X}(x) = (1,x)$.
  For a based set $(Y, y_{0})$ and a map $f \from X \to Y$, we define the extension
  $\ext{f} \from EX \to (Y, y_{0})$ of $f$ as follows.
  \begin{equation*}
    \ext{f}(u) =
    \begin{cases}
      f(x), & u = (1, x) \\
      y_{0}, & u = 0
    \end{cases}
  \end{equation*}
  That the diagram in \cref{eq:basepoint-extension-diag} commutes is clear:
  \begin{equation*}
    \ext{f}(i_{X}(x)) =
    \begin{cases}
      f(x), & i_{X}(x) = (1, x) \\
      y_{0}, & i_{X}(x) = 0
    \end{cases}
    = f(x)
  \end{equation*}
  Finally, let $g \from EX \to (Y, y_{0})$ be any other map with $g \comp i_{X} = f$.
  We have for all $u \in EX$ that
  \begin{align*}
    g(u)
    & =
    \begin{cases}
      g(u), & u = (1, x) \\
      y_{0}, & u = 0
    \end{cases}
      \tag*{because $g$ based}
    \\
    & =
    \begin{cases}
      g(i_{X}(x)), & u = (1, x) \\
      y_{0}, & u = 0
    \end{cases}
      \tag*{by def. $i_{X}$}
    \\
    & =
    \begin{cases}
      f(x), & u = (1, x) \\
      y_{0}, & u = 0
    \end{cases}
      \tag*{because $g \comp i_{X} = f$}
    \\
    & = \ext{f}(u)
      \tag*{by def. $\ext{f}$}
  \end{align*}
  and thus $g = \ext{f}$.
  Therefore, $\ext{f}$ is the unique extension of $f$ along $i_{X}$ as required by
  the lemma.
\end{proof}

The result of \cref{lem:basepoint-extension} is a typical \keyw*{unique mapping property (UMP)} that
is used to define $E$ as universal construction that adds exactly one base point to a set.
In fact, one can show $E$ cannot add a second element to the set and that there is no $x \in X$
with the property that $i_{X}(x)$ is equal to the basepoint of $EX$.
Moreover, one can prove that any choice for $E$ works, as long as it fulfils the UMP.
This can be proven in general, but also concretely for our previous extension of $\QNN$ to $D$.

\begin{exercise}
  Prove that $(D, \err) \cong E(\QNN)$ in $\BSetC$ only by using the UMP of $E$.
\end{exercise}


%%% Local Variables:
%%% ispell-local-dictionary: "british"
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "cats-functors"
%%% End: