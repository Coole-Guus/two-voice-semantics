\section{Functorial Semantics}
\label{sec:functorial-sem}

\begin{definition}{}{simp-context}
  \keyw[context]{Contexts} of \Arith{} are given by the following grammar.
  \begin{equation*}
    c \Coloneq - \mid n \mid c \oplus c \mid c \otimes c \mid c \oslash c
  \end{equation*}
  We call ``$-$'' the \keyw{hole} in a context.
  Moreover, we write $c'\subst{c}$ for the substitution of $c$ for the hole in the context $c'$,
  which is defined iteratively by
  \begin{equation*}
    -[c] = c
    \qquad n[c] = c
    \qquad (c_{1} \mathbin{\Box} c_{2})[c] = c_{1}\subst{c} \mathbin{\Box} c_{2}\subst{c},
    \text{ where } \Box \in \set{\oplus, \otimes, \oslash}
  \end{equation*}
\end{definition}

\begin{example}{}{simpl-contexts}
  Any expression is a context, as the hole does not have to be used.
  We put $c_{1} = - \oplus \Num{5}$ and $c_{2} = \Num{3} \oslash -$, then
  \begin{equation*}
    c_{1}[c_{2}][\Num{6}]
    = ((\Num{3} \oslash -) \oplus \Num{5}) [\Num{6}]
    = (\Num{3} \oslash \Num{6}) \oplus \Num{5} \, .
  \end{equation*}
\end{example}

Let us note that substitution of contexts is associative and unital in the following sense.
\begin{lemma}{}{ctx-subst-assoc-unit}
  For all contexts $c_{1}, c_{2}, c_{3}$ the following identities hold
  \begin{equation}
    \label{eq:ctx-subst-assoc}
    c_{1}[c_{2}[c_{3}]] = c_{1}[c_{2}][c_{3}]
  \end{equation}
  and
  \begin{equation}
    \label{eq:ctx-subst-unital}
    c_{1}[-] = c_{1} = -[c_{1}]
  \end{equation}
\end{lemma}
\begin{proof}
  We prove \cref{eq:ctx-subst-assoc} by induction on $c_{1}$.
  \begin{itemize}
  \item For the base case, in which we substitute for a hole, we have
    \begin{equation*}
      -[c_{2}[c_{3}]] = c_{2}[c_{3}] = -[c_{2}][c_{3}]
    \end{equation*}
    by definition.
  \item The case where we substitute into a numeral gives
    \begin{equation*}
      n[c_{2}[c_{3}]] = n = n[c_{2}][c_{3}]
    \end{equation*}
    by definition.
  \item Finally, if $\Box \in \set{\oplus, \otimes, \oslash}$ is any of the operators,
    we have
    \begin{align*}
      (c_{1} \mathbin{\Box} c_{1}')[c_{2}[c_{3}]]
      & = c_{1}[c_{2}[c_{3}]] \mathbin{\Box} c_{1}'[c_{2}[c_{3}]]
      \tag*{by def.} \\
      & = c_{1}[c_{2}][c_{3}] \mathbin{\Box} c_{1}'[c_{2}][c_{3}]
      \tag*{by induction hyp.} \\
      & = (c_{1}[c_{2}] \mathbin{\Box} c_{1}'[c_{2}])[c_{3}]
      \tag*{by def.} \\
      & = (c_{1} \mathbin{\Box} c_{1}')[c_{2}][c_{3}]
      \tag*{by def.}
    \end{align*}
  \end{itemize}
  These are all cases and \cref{eq:ctx-subst-assoc} holds thus by induction.

  The proof for \cref{eq:ctx-subst-unital} is proven analogously by induction on $c_{1}$.
\end{proof}

The \cref{lem:ctx-subst-assoc-unit} allows us the so-called \keyw{classifying category}
$\ClArith$ of $\Arith$.
This category has one object $\ast$ and
\begin{itemize}
\item $\Hom{\ClArith}(\ast, \ast) = \setDef{c}{c \text{ is context}}$
\item $\id_{\ast} = -$
\item $c_{2} \comp c_{1} = c_{2}[c_{1}]$,
\end{itemize}
in other words, composition is given by substitution and the hole acts as the identity.
Pictorially, $\ClArith$ looks like the following graph with one node and an edge
for every context.
\begin{center}
  \begin{tikzpicture}
    \node {$\ast$}
    edge [loop above] node {$-$} ()
    edge [loop right] node {$- \oplus \Num{5}$} ()
    edge [loop below] node {$\Num{3} \oslash -$} ()
    edge [loop left] node {\vdots} ();
  \end{tikzpicture}
\end{center}

\begin{exercise}
  \label{exc:classifying-cat-arith}
  Prove that $\ClArith$ is a category with the help of \cref{lem:ctx-subst-assoc-unit}.
\end{exercise}

\begin{theorem}{}{den-sem-arith-functor}
  The denotational semantics of \Arith{} give rise to a functor $\DArith \from \ClArith \to \BSet$
  with $\DArith \ast = (D, \err)$ and for a context $c \from \ast \to \ast$
  \begin{equation*}
    \begin{tikzcd}
      {\DArith \ast = D} & {\DArith \ast} \\
      \QNN
      \arrow["\DArith c", dashed, from=1-1, to=1-2]
      \arrow["i", from=2-1, to=1-1]
      \arrow["f"', from=2-1, to=1-2]
    \end{tikzcd}
    \text{ with } f(q) = \denExp{c \subst{\Num{q}}}
  \end{equation*}
\end{theorem}

\begin{exercise}
  \label{exc:den-sem-arith-functor}
  Prove that $\DArith$, as defined in \cref{thm:den-sem-arith-functor} is indeed a functor.
  Discuss why the functor laws formalise compositionality.
\end{exercise}

We can interpret any functor $F \from \ClArith \to \BSet$ as $\BSet$-valued semantics (or model)
for \Arith{}.
But then we can ask for the relation between the semantics $\DArith$ and $F$!

\begin{example}{}{bos-arith-functor}
  We can define a functor $\OArith \from \ClArith \to \BSet$ by
  $\OArith \ast = (\pow(\QNN), \emptyset)$ and on morphisms $c \from \ast \to \ast$
  for $U \subseteq \QNN$ by
  \begin{equation*}
    (\OArith c)(U) = \setDef{q \in \QNN}{c[\Num{p}] \Downarrow q, p \in U}
  \end{equation*}
  Clearly, $(\OArith c)(\emptyset) = \emptyset$ and thus $\OArith c$ is a based map.
  It is easily seen that $\OArith$ is a functor.
\end{example}

\begin{exercise}
  \label{exc:den-sem-arith-functor}
  Prove that $\OArith$ fulfils the functor laws.
\end{exercise}

This raises the question: How can we relate semantics given as functors, in particular,
$\DArith$ and $\OArith$.

%%% Local Variables:
%%% ispell-local-dictionary: "british"
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "functorial-sem"
%%% End: