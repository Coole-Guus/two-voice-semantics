\section{Natural Transformations}
\label{sec:natural-trans}

We have discussed the abstraction of mathematical relations into morphisms in categories,
then functors as morphisms between categories that are an abstraction of mathematical constructions.
The next step is to find out what morphisms between functors are, which will enable us to relate
those constructions.

Suppose that $F$ and $G$ are functors $C \to D$ between categories $C$ and $D$.
Note that these functors are parallel because the have common domain and codomain.
We wish to \emph{transform} now the construction given by $F$ into that of $G$.
Thus, for every object $a \in C$, we need a morphism $\alpha_{a} \from Fa \to Ga$ in $D$.
This tells us how the constructions are related on objects, but not on morphisms!
Additionally, $\alpha_{a}$ and $\alpha_{b}$ could wildly differ for different objects $a$
and $b$ in $C$ in more or less subtle ways, see
\cref{ex:free-basepoint-weird,ex:choice-operational-semantics} below.
Both issues are fixed by the \keyw*{naturality} condition.

\begin{definition}{}{natural-transformation}
  Let $F,G \from C \to D$ be functors.
  A \keyw{natural transformation}, denoted by $\alpha \from F \to G$ or $\alpha \from F \natTo G$,
  is a family of morphisms $\alpha_{a} \from Fa \to Ga$ in $D$ indexed by objects $a$ in $C$,
  such that for all $f \in \Hom{C}(a,b)$ the following diagram commutes.
  \begin{equation*}
    % https://q.uiver.app/?q=WzAsNCxbMCwwLCJGIGEiXSxbMSwwLCJHIGEiXSxbMCwxLCJGYiJdLFsxLDEsIkdiIl0sWzAsMSwiXFxhbHBoYV97YX0iXSxbMiwzLCJcXGFscGhhX3tifSJdLFswLDIsIkYgZiIsMl0sWzEsMywiRyBmIl1d
    \begin{tikzcd}
      {F a} & {G a} \\
      Fb & Gb
      \arrow["{\alpha_{a}}", from=1-1, to=1-2]
      \arrow["{\alpha_{b}}", from=2-1, to=2-2]
      \arrow["{F f}"', from=1-1, to=2-1]
      \arrow["{G f}", from=1-2, to=2-2]
    \end{tikzcd}
  \end{equation*}
\end{definition}

It is quite helpful to visualise natural transformations as in the following diagram.

\begin{equation*}
  % https://q.uiver.app/?q=WzAsMixbMCwwLCJDIl0sWzEsMCwiRCJdLFswLDEsIkYiLDAseyJjdXJ2ZSI6LTJ9XSxbMCwxLCJHIiwyLHsiY3VydmUiOjJ9XSxbMiwzLCJcXGFscGhhIiwwLHsic2hvcnRlbiI6eyJzb3VyY2UiOjIwLCJ0YXJnZXQiOjIwfX1dXQ==
  \begin{tikzcd}
    C & D
    \arrow[""{name=0, anchor=center, inner sep=0}, "F", curve={height=-12pt}, from=1-1, to=1-2]
    \arrow[""{name=1, anchor=center, inner sep=0}, "G"', curve={height=12pt}, from=1-1, to=1-2]
    \arrow["\alpha", shorten <=3pt, shorten >=3pt, Rightarrow, from=0, to=1]
  \end{tikzcd}
\end{equation*}
From this diagram, it is immediately clear that $F$ and $G$ are parallel functors and that
$\alpha$ is a morphism between them.

Let us now return to our quest to relate the denotational and operational semantics.
\begin{theorem}{}{arith-den-op-map}
  There is a natural transformation $\sigma \from \DArith \to \OArith$.
\end{theorem}
\begin{proof}
  We have to produce $\sigma$ as in the following diagram.
  \begin{equation*}
    \begin{tikzcd}
      \ClArith & \BSet
      \arrow[""{name=0, anchor=center, inner sep=0}, "\DArith", curve={height=-12pt}, from=1-1, to=1-2]
      \arrow[""{name=1, anchor=center, inner sep=0}, "\OArith"', curve={height=12pt}, from=1-1, to=1-2]
      \arrow["\sigma", shorten <=3pt, shorten >=3pt, from=0, to=1]
    \end{tikzcd}
  \end{equation*}
  Since $\ClArith$ has only one object $\ast$, we have to define a based map
  $\sigma_{\ast} \from \DArith \ast \to \OArith \ast$, which we do by extension as in the following
  diagram.
  \begin{equation*}
    \begin{tikzcd}
      {U(\DArith \ast) = D} & {\pow(\QNN) = U(\OArith \ast)} \\
      \QNN
      \arrow["\sigma_{\ast}", dashed, from=1-1, to=1-2]
      \arrow["i", from=2-1, to=1-1]
      \arrow["\eta"', from=2-1, to=1-2]
    \end{tikzcd}
    \text{ with } \eta(q) = \set{q}
  \end{equation*}
  To prove naturality of $\sigma$, we have to prove that the following diagram commutes
  for all contexts $c$.
  \begin{equation*}
    % https://q.uiver.app/?q=WzAsNCxbMCwwLCJEIl0sWzEsMCwiXFxQb3coXFxRTk4pIl0sWzAsMSwiRCJdLFsxLDEsIlxcUG93KFxcUU5OKSJdLFswLDEsIlxcc2lnbWFfe1xcYXN0fSJdLFsyLDMsIlxcc2lnbWFfe1xcYXN0fSJdLFswLDIsIlxcRFNpbXAgYyIsMl0sWzEsMywiXFxPU2ltcCBjIl1d
    \begin{tikzcd}
      D & {\pow(\QNN)} \\
      D & {\pow(\QNN)}
      \arrow["{\sigma_{\ast}}", from=1-1, to=1-2]
      \arrow["{\sigma_{\ast}}", from=2-1, to=2-2]
      \arrow["{\DArith c}"', from=1-1, to=2-1]
      \arrow["{\OArith c}", from=1-2, to=2-2]
    \end{tikzcd}
  \end{equation*}
  For $p \in \QNN$, this means that we have to show that the following identity
  marked with ``?'' holds.
  \begin{equation}
    \label{eq:arith-den-op-naturality}
    (\OArith c)(\sigma_{\ast}(p))
    = (\OArith c)\set{p}
    = \setDef{q}{c[\Num{p}] \Downarrow q}
    \overset{?}{=}
    \sigma_{\ast}(\denExp{c \subst{\Num{p}}})
  \end{equation}
  We will be able to finish the proof after the following lemma.
\end{proof}

One of the central goals of the study of semantics is to formally relate various
different semantics, and usually these relations are formulated in the form of lemmas
such as the following.
This lemma will be exactly what we need to finish the naturality proof.
\begin{lemma}{}{arith-den-op-sem-relation}
  For all contexts $c$ and $p \in \QNN$, the following holds.
  \begin{enumerate}
  \item \label{item:arith-op-error}
    If $\denExp{c \subst{\Num{p}}} = \err$, then there is no $q \in \QNN$ with
    $c \subst{\Num{p}} \Downarrow q$.
  \item \label{item:arith-op-value}
    If $\denExp{c \subst{\Num{p}}} \in \QNN$, then
    $c \subst{\Num{p}} \Downarrow \denExp{c \subst{\Num{p}}}$.
  \item \label{item:arith-op-deterministic}
    The big-step operational semantics is \keyw{deterministic}:
    For all $q, q' \in \QNN$, if $c \subst{\Num{p}} \Downarrow q$ and
    $c \subst{\Num{p}} \Downarrow q'$, then $q = q'$.
  \end{enumerate}
\end{lemma}

\begin{exercise}
  Prove \cref{lem:arith-den-op-sem-relation}.
  Hint: prove first \cref{item:arith-op-deterministic}, and
  then 1 and 2 together by induction on the context $c$.
\end{exercise}

Let us now finish the proof of naturality.
\begin{proof}[Remaining proof of \cref{thm:arith-den-op-map}]
  We have to show that \cref{eq:arith-den-op-naturality} holds, which we do by distinguishing
  the two cases for the result of the denotational semantics:
  \begin{enumerate}
  \item If $\denExp{c \subst{\Num{p}}} = \err$, then by \cref{item:arith-op-deterministic} of
    \cref{lem:arith-den-op-sem-relation} $(\OArith c)\set{p} = \emptyset$.
    Since we defined $\sigma_{\ast}$ to be a based map, we also have
    $\sigma_{\ast}(\denExp{c \subst{\Num{p}}}) = \ast$ and we get that \cref{eq:arith-den-op-naturality}
    holds in this case.
  \item Otherwise, we have $\denExp{c \subst{\Num{p}}} \in \QNN$ and by
    \cref{item:arith-op-value,item:arith-op-deterministic} we get that
    $(\OArith c)\set{p} = \set{\denExp{c \subst{\Num{p}}}}
    = \eta(\denExp{c \subst{\Num{p}}})
    = \sigma_{\ast}(\denExp{c \subst{\Num{p}}})$
    and thus \cref{eq:arith-den-op-naturality} also holds in this case.
  \end{enumerate}
  This covers the two possible cases and $\sigma$ is natural.
\end{proof}

The second case in the proof of \cref{thm:arith-den-op-map} is particularly noteworthy.
In fact, we show the equality of two sets by proving two inclusions:
$\sigma_{\ast}(\denExp{c \subst{\Num{p}}}) \subseteq (\OArith c)\set{p}$
and
$(\OArith c)\set{p} \subseteq \sigma_{\ast}(\denExp{c \subst{\Num{p}}})$.
The first inclusion is the statement of \cref{item:arith-op-value} in
\cref{lem:arith-den-op-sem-relation}, and is often referred to as \keyw*{soundness}
of the denotational semantics.
For the second inclusion to hold, we need that $(\OArith c)\set{p}$ is a singleton for error-free
computations, which is the content of \cref{item:arith-op-deterministic}.


\begin{example}{}{free-basepoint-weird}
\end{example}

\begin{example}{}{choice-operational-semantics}
\end{example}