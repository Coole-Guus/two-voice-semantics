# Two-Voice Semantics

## Name
Two-Voice Semantics: An Introduction to Program Semantics and Category Theory

## Description

These are the lecture notes for the course Computational Models and Semantics (4343CMS6X)
in the computer science master programme of the Leiden University.

## Compiled Document

Where will the compiled PDF be deployed?

## Usage and Compilation

The main document has to be compiled with XeLaTex.
To display the correct git information, the hooks in *git-hooks* have to be copied into the
directory *.git/hooks*:

    > cp git-hooks/* .git/hooks/

## Contributing

If you find any mistakes or have any suggestions, please open an issue here on gitlab.

## Authors and Acknowledgement

I, Henning Basold, would like to thank the students, who participated in the course
Computational Models and Semantics, for their feedback.

## License

The lecture notes are licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

## Project status

The lecture notes are currently being written.
