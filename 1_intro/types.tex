\section{Types for \Arith{}}
\label{sec:types-arith}

The idea of type systems is that they provide syntactic means to prove the absence of some,
typically unwanted, behaviour in computer programs~\cite{Pierce:TPL}.
However, type systems can also be used for program optimisation and to track other static
information of programs, such as
termination~\cite{Abel:SizedTypes,Ghilezan96:StrongNormalizationTypability},
program complexity~\cite{DalLago22:ImplicitComputationComplexity}, or
communication protocols~\cite{Huttel16:FoundationsSessionTy}.

In the case of \Arith{}, we wish to rule out division by zero.
We will achieve this by assigning to every expression one of two types that are given by
the following grammar.
\begin{equation*}
  A \Coloneq \TN \mid \TNP
\end{equation*}
The idea is that expressions of type $\TN$ will be generally \emph{numbers}, while expressions
of type $\TNP$ are positive numbers.
Similar to natural deduction, we present the type system as a collection of rules that allow
the derivation of the judgement
\begin{equation*}
  \JArith{e}{A}
\end{equation*}
for an expression $e$ and a type $A$.
This judgement should be read as ``$e$ is an expression of type $A$''.
The type system is in given in \cref{fig:types-arith}.
\begin{figure}[ht]
  \begin{gather*}
    \begin{prooftree}[center=false]
      \infer0[(Const)]{\JArithN{\Num{0}}}
    \end{prooftree}
    \qquad
    \begin{prooftree}[center=false]
      \hypo{a > 0}
      \infer1[(PosConst)]{\JArithNP{\Num{a}}}
    \end{prooftree}
    \qquad
    \begin{prooftree}[center=false]
      \hypo{\JArithNP{e}}
      \infer1[(Sub)]{\JArithN{e}}
    \end{prooftree}
    \\[7pt]
    \begin{prooftree}
      \hypo{\JArith{e_{1}}{A}}
      \hypo{\JArith{e_{2}}{A}}
      \infer2[(Plus)]{\JArith{e_{1} \oplus e_{2}}{A}}
    \end{prooftree}
    \qquad
    \begin{prooftree}
      \hypo{\JArith{e_{1}}{A}}
      \hypo{\JArith{e_{2}}{A}}
      \infer2[(Mul)]{\JArith{e_{1} \otimes e_{2}}{A}}
    \end{prooftree}
    \qquad
    \begin{prooftree}
      \hypo{\JArith{e_{1}}{A}}
      \hypo{\JArithNP{e_{2}}}
      \infer2[(Div)]{\JArith{e_{1} \oslash e_{2}}{A}}
    \end{prooftree}
  \end{gather*}
  \caption{A Type System for \Arith{}}
  \label{fig:types-arith}
\end{figure}
This type system has 6 rules, two for constants and positive constants, one for each
operation and one \keyw*{subtyping} rule (Sub).
With the subtyping rule we can weaken information that we have gathered about an expression.
Semantically, the (Sub) corresponds to the inclusion of positive into non-negative rational number,
as we will see later\todo{ref}.

Since all rules, except (Sub) are clear from the context of their use, we will generally leave out
the rule labels in derivations.
Also the hypothesis of the rule (PosConst) will be left out, as it can be inferred from the context.

\begin{example}{}{arith-typing}
  Let us give some example derivations.
  The first is a self-explanatory derivation that $(\Num{2} \oplus \Num{1/3}) \oslash \Num{3}$
  is a positive numeral.
  \begin{equation*}
    \begin{prooftree}
      \infer0{\JArithNP{\Num{1/3}}}
      \infer0{\JArithNP{\Num{1/3}}}
      \infer2{\JArithNP{\Num{2} \oplus \Num{1/3}}}
      \infer0{\JArithNP{\Num{3}}}
      \infer2{\JArithNP{(\Num{2} \oplus \Num{1/3}) \oslash \Num{3}}}
    \end{prooftree}
  \end{equation*}
  If we want to combine $\Num{0}$ with any positive numeral, then we have to use the subtyping
  rule as in the following derivation.
  \begin{equation*}
    \begin{prooftree}
      \infer0{\JArithN{\Num{0}}}
      \infer0{\JArithNP{\Num{3}}}
      \infer1[(Sub)]{\JArithN{\Num{3}}}
      \infer2{\JArithN{\Num{0} \oplus \Num{3}}}
    \end{prooftree}
  \end{equation*}
\end{example}

Since $\Num{0}$ only has the type $\TN$, it is impossible to derive $\JArith{e \oslash \Num{0}}{A}$
for any expression $e$ and type $A$.
\begin{theorem}{}{arith-div-zero-no-type}
  For all expressions $e$ and types $A$ it is not possible to derive
  $\JArith{e \oslash \Num{0}}{A}$.
\end{theorem}
\begin{proof}
  We prove the theorem by induction on type derivations, which are trees constructed from the rules
  in \cref{fig:types-arith}.
  Explicitly, we prove for all $e$, $A$ and derivation trees $T$
  that $T$ cannot derive $\JArith{e \oslash \Num{0}}{A}$.
  The base cases are (Const) and (PosConst), which do not apply to expressions with division and
  thus lead immediately to a contradiction.
  In the induction step, $T$ is constructed by the application of a rule to smaller derivation
  trees.
  If the derivation ends with (Sub) as last rule, then $T$ is given by applying (Sub) to a tree
  $T'$ that derives $\JArithNP{e}$ and we obtain a contradiction by applying the induction
  hypothesis to the tree $T'$.
  Similar to the base cases, (Plus) and (Mul) do not apply to expressions with division.
  Finally, suppose that $T$ is given by (Div) applied to derivation trees $T_{1}$ and $T_{2}$ for,
  respectively, $\JArith{e}{A}$ and $\JArithNP{\Num{0}}$.
  To complete the proof, one has to show that $\JArithNP{\Num{0}}$ is not derivable, which is
  left as an exercise.
  Thus, by induction, no tree can be a derivation for $\JArith{e \oslash \Num{0}}{A}$ in the
  type system of \Arith{}.
\end{proof}

\printexercise{exercise}{zero-not-positive-type}


%%% Local Variables:
%%% ispell-local-dictionary: "british"
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "intro"
%%% End:
