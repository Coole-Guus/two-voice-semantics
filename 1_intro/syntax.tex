\section{The Syntax of \Arith{}}
\label{sec:syntax-arith}

We start with the \keyw*{syntax} of the language \Arith{}, which describes formally
the valid programs of the language.
The syntax of a language can consist of various components, the naming of which is imprecise
and varies by community and author.
In formal semantics, we typically refer to a program that represents by itself a value, like
a number, as \keyw*{expression}, thus it \emph{expresses} this value.
Contrary to this, we will see later that imperative languages also have \keyw*[command]{commands}
that do not represent a value but rather \emph{command} the machine to execute some action.

\begin{definition}{}{simp-expr}
  Let $\QNN$ be the set of rational numbers greater than or equal to $0$.
  \keyw[expression]{Expressions} of \Arith{} are given by the following grammar.
  \begin{equation*}
    e \Coloneq \Num{a} \mid e \oplus e \mid e \otimes e \mid e \oslash e \, ,
    \quad \text{ where } a \in \QNN
  \end{equation*}
  We denote by $\Exp$ the set of all expressions $e$ and by $\NumS$ the
  set of \keyw[numeral]{numerals}:
  \begin{equation*}
    \NumS = \setDef{\Num{a}}{a \in \QNN} \, .
  \end{equation*}
\end{definition}

In \cref{def:simp-expr}, the expressions are given by a \keyw{context-free grammar}
in Backus-Naur form.
The way to read it is as follows.
An expression $e$ has the shape of any of the options separated by vertical bars on the right-hand
side of $\Coloneq$, in which $e$ can be again recursively unfolded.
There are four options to generate expressions.
The first option is that of a numeral, a number seen as pure syntax.
Next, we can combine two expressions by putting a $\oplus$ between them.
This is the syntactic representation of addition.
The remaining two options allow us to similarly form expressions with multiplication and
division.
Note that any expression represents an arithmetic combination of numbers that represents
a constant but is \emph{unevaluated}.
Our task will be to give semantics to expressions and evaluate them to numbers.

Before we get there, let us record some examples of arithmetic expressions in \Arith{}.

\begin{example}{}{arith-exp}
  Since $\NumS \subset \Exp$, any numeral, such as $\Num{0}$, $\Num{2}$ or $\Num{1/3}$, is an
  expression.
  We can combine these into $\Num{2} \oplus \Num{1/3}$ or
  $(\Num{2} \oplus \Num{1/3}) \otimes \Num{3}$.
  Also, division by zero is allowed, since $\Num{2} \oslash \Num{0}$ is an expression.
\end{example}

It is important to remember that expressions are meaningless without semantics.
In particular, equality of expressions is just a comparison of symbols and thus
$\Num{2} \oplus \Num{1/3} \neq \Num{1/3} \oplus \Num{2}$!

Giving semantics to arithmetic expressions is, in principle, quite straightforward except
that potential division by zero causes some trouble.
There are two ways to handle the issue: either we rule out division by zero through a static
check, for example by a \keyw{type system}, or we allow \keyw{failure} in the semantics.
We begin with type systems and then proceed to the semantic approach.

%%% Local Variables:
%%% ispell-local-dictionary: "british"
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "intro"
%%% End:
