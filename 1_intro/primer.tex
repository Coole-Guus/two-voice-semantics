\section{A Zoo of Computational Models and Semantics}
\label{sec:simp-primer}

\todo[inline]{Add diagram}

\section{Small-Step Semantics for Arith{}}
\label{sec:small-step-arith}

We define a transition relation on expressions
\begin{equation*}
  e_{1} \stepArith e_{2}
\end{equation*}
that should be read as ``$e_{1}$ makes one computation step to reach $e_{2}$''.
It is convenient to define small-step semantics by using an auxiliary relation,
called \keyw*{contraction},
\begin{equation*}
  e_{1} \succ e_{2}
\end{equation*}
to model computations at the top-level of the expression $e_{1}$.
We define both relations by the rules in \cref{fig:rules-small-step-arith}.

\begin{figure}[ht]
  \begin{gather*}
    \begin{prooftree}[center=false]
      \infer0{\Num{a} \oplus \Num{b} \contrArith \Num{a + b}}
    \end{prooftree}
    \qquad
    \begin{prooftree}[center=false]
      \infer0{\Num{a} \otimes \Num{b} \contrArith \Num{a \cdot b}}
    \end{prooftree}
    \qquad
    \begin{prooftree}[center=false]
      \hypo{b > 0}
      \infer1{\Num{a} \oslash \Num{b} \contrArith \Num{a / b}}
    \end{prooftree}
    \qquad
    \begin{prooftree}[center=false]
      \hypo{e \contrArith e'}
      \infer1{e \stepArith e'}
    \end{prooftree}
    \\[7pt]
    \begin{prooftree}
      \hypo{e_{1} \stepArith e'_{1}}
      \infer1{e_{1} \oplus e_{2} \stepArith e'_{1} \oplus e_{2}}
    \end{prooftree}
    \qquad
    \begin{prooftree}
      \hypo{e_{2} \stepArith e'_{2}}
      \infer1{e_{1} \oplus e_{2} \stepArith e'_{1} \oplus e_{2}}
    \end{prooftree}
    \qquad
    \begin{prooftree}
      \hypo{e_{1} \stepArith e'_{1}}
      \infer1{e_{1} \otimes e_{2} \stepArith e'_{1} \otimes e_{2}}
    \end{prooftree}
    \qquad
    \begin{prooftree}
      \hypo{e_{2} \stepArith e'_{2}}
      \infer1{e_{1} \otimes e_{2} \stepArith e'_{1} \otimes e_{2}}
    \end{prooftree}
    \\[7pt]
    \begin{prooftree}
      \hypo{e_{1} \stepArith e'_{1}}
      \infer1{e_{1} \oslash e_{2} \stepArith e'_{1} \oslash e_{2}}
    \end{prooftree}
    \qquad
    \begin{prooftree}
      \hypo{e_{2} \stepArith e'_{2}}
      \infer1{e_{1} \oslash e_{2} \stepArith e'_{1} \oslash e_{2}}
    \end{prooftree}
  \end{gather*}
  \caption{Rules of small-step semantics for \Arith{}}
  \label{fig:rules-small-step-arith}
\end{figure}
The rules in the first row carry out the arithmetical operation with the
contraction relation~$\contrArith$ on the top-level of expressions and use these in the small-step
semantics relation~$\stepArith$.
In the second and third row, we find the rules that allow us to embed these calculations into a
\keyw*{context}.
These 6 rules are also called \keyw*{congruence} rules.
We will see later\todo{Ref} how the congruence rules can be obtained in a more principled way.

\begin{example}{}{small-step-arith}
  To evaluate the expression $(\Num{2} \oplus \Num{1/3}) \otimes \Num{3}$, we have to make
  two computation steps.
  First step:
  \begin{equation*}
    \begin{prooftree}
      \infer0{\Num{2} \oplus \Num{1/3} \stepArith \Num{7/3}}
      \infer1{(\Num{2} \oplus \Num{1/3}) \otimes \Num{3} \stepArith \Num{7/3} \otimes \Num{3}}
    \end{prooftree}
  \end{equation*}
  Second step:
  \begin{equation*}
    \begin{prooftree}
      \infer0{\Num{7/3} \otimes \Num{3} \stepArith \Num{7}}
    \end{prooftree}
  \end{equation*}
\end{example}

Note that there is no derivation of $\Num{k} \oslash \Num{0} \stepArith e$ for any numeral $\Num{k}$
and expression $e$.
This can be also inferred from the type system in \cref{sec:types-arith} by proving that computation
steps preserve types, which is also called subject reduction.

\begin{theorem}{Type preservation/subject reduction}{type-pres-small-step-arith}
  If $\JArith{e}{A}$ and $e \stepArith e'$, then $\JArith{e'}{A}$.
\end{theorem}

\printexercise{exercise}{type-pres-small-step-arith}

\section{Big-Step Semantics for Arith{}}
\label{sec:big-step-arith}

Where small-step semantics presents computation steps as a sequence of individual transition steps,
big-step semantics presents these as derivation trees.
The ``big'' in its name comes from the fact that it is given by a relation
\begin{equation*}
  e \bigArith a \, , \qquad e \in \Exp , a \in \QNN
\end{equation*}
that relates an expression $e$ with a \keyw*{value} $a$ by combining several steps into bigger steps
in a computation.
The notion of values that are allowed as result depend on the language at hand.
Here we work with numbers, but other languages may use truth values to interpret Boolean expressions
or even functions to interpret programs with input.

The big-step semantics for \Arith{} are given in \cref{fig:rules-big-step-arith}.
\begin{figure}[ht]
  \begin{gather*}
    \begin{prooftree}[center=false]
      \infer0{\Num{a} \bigArith a}
    \end{prooftree}
    \qquad
    \begin{prooftree}[center=false]
      \hypo{e_{1} \bigArith a_{1}}
      \hypo{e_{2} \bigArith a_{2}}
      \infer2{e_{1} \oplus e_{2} \bigArith a_{1} + a_{2}}
    \end{prooftree}
    \qquad
    \begin{prooftree}[center=false]
      \hypo{e_{1} \bigArith a_{1}}
      \hypo{e_{2} \bigArith a_{2}}
      \infer2{e_{1} \otimes e_{2} \bigArith a_{1} \cdot a_{2}}
    \end{prooftree}
    \qquad
    \begin{prooftree}[center=false]
      \hypo{e_{1} \bigArith a_{1}}
      \hypo{e_{2} \bigArith a_{2}}
      \hypo{a_{2} > 0}
      \infer3{e_{1} \oslash e_{2} \bigArith a_{1} / a_{2}}
    \end{prooftree}
  \end{gather*}
  \caption{Rules of big-step semantics for \Arith{}}
  \label{fig:rules-big-step-arith}
\end{figure}
An evaluation of an expression $e$ is given by derivation of $e \bigArith a$.

\begin{example}{}{big-step-arith}
  We evaluate the expression $(\Num{2} \oplus \Num{1/3}) \oslash \Num{3}$.
  \begin{equation*}
    \begin{prooftree}
      \infer0{\Num{2} \bigArith 2}
      \infer0{\Num{1/3} \bigArith 1/3}
      \infer2{\Num{2} \oplus \Num{1/3} \bigArith (2 + 1/3) = 7/3}
      \infer0{\Num{3} \bigArith 3}
      \infer2{(\Num{2} \oplus \Num{1/3}) \otimes \Num{3} \bigArith 7/3 \cdot 3 = 7}
    \end{prooftree}
  \end{equation*}
\end{example}

No number $a$ will be related to an expression $e \oslash \Num{0}$ in the big-step
semantics.
The other way around, any well-typed expression can be successfully evaluated.
\begin{theorem}{Soundness of types for big-step semantics}{type-soundness-big-arith}
  If $\JArith{e}{A}$ for some type $A$, then there is $a \in \QNN$ with
  $e \bigArith a$.
\end{theorem}

\printexercise{exercise}{type-soundness-big-arith}

By \cref{thm:arith-div-zero-no-type} it is thus not possible to derive
$e \oslash \Num{0} \bigArith a$ for any $e \in \Exp$ and $a \in \QNN$.

\section{Denotational Semantics for Arith{}}
\label{sec:denotational-arith}

The general pattern in denotational semantics is that expressions are mapped to their
\keyw*{denotation}, which is an element of the domain in which we wish to interpret
expressions.
In the case of \Arith{}, we would wish the domain to be the positive and non-negative rational
numbers.
However, to define a map $\Exp \to \QNN$, we would also have to map expressions of the form
$e \oslash \Num{0}$ to some rational number.
This is not possible, unless we map such expression to a fixed constant.
Mapping to a fixed constant $a \in \QNN$ is, unfortunately, not a good solution because
we will have that the expressions $\Num{a}$ and $e \oslash \Num{0}$ will have the same
denotation and thus such an interpretation would not agree with the small- or big-step semantics.
We need thus a better approach.

There are essentially two ways out: either we take types into account or we introduce failure
into the denotational semantics.

Let us start with denotational semantics on typed expressions.
We define the set
\begin{equation*}
  \Exp_{A} = \setDef{T}{T \text{ derivation for } \JArith{e}{A}}
\end{equation*}
with an interpretation of our types as sets of numbers
\begin{equation*}
  \sem{\TN} = \QNN
  \qquad
  \sem{\TNP} = \QP
\end{equation*}
and two maps\todo{Explain origin of name ``Scott brackets''}
\begin{equation*}
  \denArithTN{-} \from \Exp_{\TN} \to \sem{\TN}
  \qquad
  \denArithTNP{-} \from \Exp_{\TNP} \to \sem{\TNP}
\end{equation*}
with the intent that the hyphen will be replaced by the argument.
These maps are given by iterating over the derivation as in the following equations.
In the argument for each case, we indicate the last step of a derivation by adding
``by (Rule)'' to the argument.
\begin{align*}
  \denArithTN{\JArith{e}{\TN} \text{ by (Sub)}} & = \denArithTNP{\JArithNP{e}} \\
  \denArithTN{\JArith{\Num{0}}{\TN} \text{ by (Const)}} & = 0 \\
  \denArithTN{\JArith{e_{1} \oplus e_{2}}{\TN} \text{ by (Plus)}}
  & = \denArithTN{\JArith{e_{1}}{\TN}} + \denArithTN{\JArith{e_{2}}{\TN}} \\
  \denArithTN{\JArith{e_{1} \otimes e_{2}}{\TN} \text{ by (Mul)}}
  & = \denArithTN{\JArith{e_{1}}{\TN}} \cdot \denArithTN{\JArith{e_{2}}{\TN}} \\
  \denArithTN{\JArith{e_{1} \oslash e_{2}}{\TN} \text{ by (Div)}}
  & = \denArithTN{\JArith{e_{1}}{\TN}} \mathbin{/} {\denArithTN{\JArith{e_{2}}{\TNP}}}
  \\[7pt]
  \denArithTNP{\JArith{\Num{a}}{\TNP} \text{ by (PosConst)}} & = a \\
  \denArithTN{\JArith{e_{1} \oplus e_{2}}{\TNP} \text{ by (Plus)}}
  & = \denArithTN{\JArith{e_{1}}{\TNP}} + \denArithTN{\JArith{e_{2}}{\TNP}} \\
  \denArithTN{\JArith{e_{1} \otimes e_{2}}{\TNP} \text{ by (Mul)}}
  & = \denArithTN{\JArith{e_{1}}{\TNP}} \cdot \denArithTN{\JArith{e_{2}}{\TNP}} \\
  \denArithTN{\JArith{e_{1} \oslash e_{2}}{\TNP} \text{ by (Div)}}
  & = \denArithTN{\JArith{e_{1}}{\TNP}} \mathbin{/} {\denArithTN{\JArith{e_{2}}{\TNP}}}
\end{align*}

Since the result of $\denArithTNP{-}$ is always a positive number, the cases involving division
in these equations are well-defined.

The second approach to interpret \Arith{}-expressions is by allowing failures to happen.
As discussed above, we need to separate the failures from number.
This can be achieved by using a new element, say, ``$\err$'', in our domain for interpreting
expressions.
We define thus
\begin{equation*}
  D = \QNN \cup \set{\err}
\end{equation*}
and then define directly a map
\begin{equation*}
  \denArith{-} \from \Exp \to D
\end{equation*}
by iterating over (untyped) expressions as in the following equations.
\begin{align*}
  \denArith{\Num{a}} & = a \\
  \denArith{e_{1} \oplus e_{2}}
   & =
     \begin{cases}
       a_{1} + a_{2}, & \denArith{e_{k}} = a_{k} \in \QNN \\
       \err, & \text{otherwise}
     \end{cases}
  \\
  \denArith{e_{1} \otimes e_{2}}
   & =
     \begin{cases}
       a_{1} \cdot a_{2}, & \denArith{e_{k}} = a_{k} \in \QNN \\
       \err, & \text{otherwise}
     \end{cases}
  \\
  \denArith{e_{1} \oslash e_{2}}
   & =
     \begin{cases}
       a_{1} / {a_{2}}, & \denArith{e_{k}} = a_{k} \in \QNN \text{ and } a_{2} > 0 \\
       \err, & \text{otherwise}
     \end{cases}
\end{align*}

Again, these equations are obviously well-defined.
It should be noted if a failure occurs anywhere in the evaluation, then it is propagated.

\begin{example}{}{den-arith-division-zero}
  We evaluate $(\Num{2} \oslash \Num{0}) \otimes \Num{0}$.
  First of all, we have
  \begin{equation*}
    \denArith{\Num{2} \oslash \Num{0}} = \err
  \end{equation*}
  because $\denArith{\Num{0}} = 0$.
  Then, by the case for multiplication, we have
  \begin{equation*}
    \denArith{(\Num{2} \oslash \Num{0}) \otimes \Num{0}} = \err
  \end{equation*}
  because the left-hand side of the multiplication fails.
\end{example}

By its very definition, $\denArithT{A}{-}$ will play well with types.
Analogously to \cref{thm:type-soundness-big-arith}, the types are also sound for the semantics
with failure.

\begin{theorem}{}{types-sound-arith-failure-den}
  If $\JArith{e}{A}$ is derivable for some type $A$, then $\denArith{e} \in \QNN$.
\end{theorem}

\printexercise{exercise}{types-sound-arith-failure-den}

In what is to come, we will investigate how the various semantics relate for one language and
across lanuages:
\begin{itemize}
\item Do the small-step, big-step and denotational semantics agree?
\item Can expressions with the same semantics be used interchangeably inside other expressions?
\item What features of computational models correspond to more complex programming languages?
\end{itemize}
Particular focus for the last question will be on recursion, as in while-loops and recursive calls
in functional languages.

%%% Local Variables:
%%% ispell-local-dictionary: "british"
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "intro"
%%% End:
